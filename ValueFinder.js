// ==UserScript==
// @name         Steam Auction Value Finder
// @version      0.1
// @description  Description
// @author       James Winter
// @match        http://store.steampowered.com/search/?auction=1
// @grant        GM_xmlhttpRequest
// @require      http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js
// ==/UserScript==
$(document).ready(function(){
    var gemSackPrice = 0.03; // last market value as of the writing of this code
    var gemSackCount = 1000;
    
    GM_xmlhttpRequest({
        method: "GET",
        url: "http://steamcommunity.com/market/search?q=sack+of+gems",
        onload: function(response){
        	var foundElement = $(response.responseText).find(".market_listing_row:contains('Sack of Gems') .market_table_value span");
        
            if (foundElement){
                var rawValue = foundElement.html();
                var trimmedValue = rawValue.replace('$','');
                var foundPrice = parseFloat(trimmedValue);
                
                if (foundPrice != 0)
                	gemSackPrice = foundPrice;
            }
            
            $('.auction_topbid_value').each(function(){
                var rawValue = $(this).html();
                var trimmedValue = rawValue.replace('Gems', '').replace(/,/g, '');
                var gemValue = parseFloat(trimmedValue);
                var dollarValue = gemValue/gemSackCount*gemSackPrice;
                    
                $(this).closest('.auction_topbid').append("<div>$" + dollarValue.toFixed(2) + "</div>");
    		});
        }
	});
});